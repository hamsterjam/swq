<?php

if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
include_once ROOT.'php/SimpleManipulator.php';

/*
 * !!!WARNING!!!
 *
 * This contains some very low level database manipulations, you should only use this
 * with ADMIN privelages
 */

class DatabaseManipulator extends SimpleManipulator {
    private string $getSource;
    private string $changeSource;
    private string $addSource;

    public function __construct(PDO $pdo, string $tableName, array $colNames) {
        parent::__construct($pdo);

        // Doesn't need colNames
        $getSource = <<<SQL
            SELECT * FROM "$tableName"
            ORDER BY id;\n
        SQL;

        // Need to generate based on colNames
        $changeSource = <<<SQL
            UPDATE "$tableName"
            SET
        SQL;

        // Need to generate based on colNames
        $addSource = <<<SQL
            INSERT INTO "$tableName"(
        SQL;

        foreach ($colNames as $colName) {
            $changeSource .= "\n".str_repeat(' ', 8)."\"$colName\" = :$colName,";
            $addSource    .= "\n".str_repeat(' ', 8)."\"$colName\",";
        }

        $changeSource = rtrim($changeSource, ",")."\n";
        $addSource    = rtrim($addSource,    ",")."\n";

        $changeSource .= <<<SQL
            WHERE
                "id" = :id
            RETURNING *;\n
        SQL;

        $addSource .= <<<SQL
            )
            SELECT
        SQL;

        foreach($colNames as $colName) {
            $addSource .= "\n".str_repeat(' ', 8).":$colName,";
        }

        $addSource = rtrim($addSource, ",\n")."\n";
        $addSource .= <<<SQL
            RETURNING *;\n
        SQL;

        $this->getSource    = $getSource;
        $this->changeSource = $changeSource;
        $this->addSource    = $addSource;

        $this->handlers['get']    = function($data) {return $this->getAll($data);};
        $this->handlers['change'] = function($data) {return $this->changeEntry($data);};
        $this->handlers['add']    = function($data) {return $this->addNew($data);};
    }

    public function getAll($data) {
        $getAll = $this->pdo->query($this->getSource);
        return $getAll->fetchAll();
    }

    public function changeEntry($data) {
        $changeEntry = $this->pdo->prepare($this->changeSource);
        foreach ($data as $key => &$value) {
            $changeEntry->bindParam(":$key", $value);
        }
        $changeEntry->execute();

        return $changeEntry->fetchAll();
    }

    public function addNew($data) {
        $addNew = $this->pdo->prepare($this->addSource);
        foreach($data as $key => &$value) {
            $addNew->bindParam(":$key", $value);
        }
        $addNew->execute();

        return $addNew->fetchAll();
    }
}

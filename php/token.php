<?php

if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
define('keylocation', ROOT.'secret/tokensign.key');
define('tokenduration', 10 * 60); // 10 minutes

function generateToken($dataObj) : string  {
    $keyfile = fopen(keylocation, 'rb');
    $key = fread($keyfile, 64);
    fclose($keyfile);

    $now = (new DateTime())->getTimestamp();

    $dataObj['from'] = $now;
    $dataObj['till'] = $now + tokenduration;
    $data = base64url_encode(json_encode($dataObj));

    $signature = base64url_encode(hash_hmac('sha256', $data, $key, true));

    return $data . '.' . $signature;
}

function validateToken(string $token , &$dataOut) : bool {
    $keyfile = fopen(keylocation, 'rb');
    $key = fread($keyfile, 64);
    fclose($keyfile);

    $now = (new DateTime())->getTimestamp();

    $fields = explode('.', $token);
    $data = $fields[0];
    $signature = $fields[1];

    if (base64url_decode($signature) != hash_hmac('sha256', $data, $key, true)) {
        return false;
    }

    $dataOut = json_decode(base64url_decode($data), true);
    if ($dataOut['till'] < $now) {
        return false;
    }

    return true;
}

function base64url_encode(string $in) : string {
    return str_replace(['+','/','='], ['-','_',''], base64_encode($in));
}

function base64url_decode(string $in) : string {
    return base64_decode(str_replace(['-','_'], ['+','/'], $in));
}

<?php

function setupPDO(&$pdo) {
    $host    = 'localhost';
    $db      = 'swq';
    $user    = 'hamsterjam';
    $pass    = '';
    $port    = 5432;

    $options = [
        \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES   => false
    ];

    $dsn = "pgsql:host=$host;port=$port;dbname=$db";

    $pdo = new \PDO($dsn, $user, $pass, $options);
}

$pdo;
setupPDO($pdo);

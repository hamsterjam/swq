<?php

if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
include_once ROOT.'php/authenticate.php';

class SimpleManipulator {
    protected array $handlers = [];
    protected PDO $pdo;

    public function __construct(PDO $pdo) {
        $this->pdo = $pdo;
    }

    public function handle($type, $data) {
        if (isset($this->handlers[$type])) {
            return $this->handlers[$type]($data);
        }

        http_response_code(400); // Bad Request
        echo "Unkown request type: $type";
        exit();
    }

    public function autoHandle() {
        $input = json_decode(file_get_contents('php://input'), true);
        $type = $input['type'];
        $data = isset($input['data']) ? $input['data'] : null;

        $ret = $this->handle($type, $data);

        reissueToken();
        echo json_encode($ret);
    }
}

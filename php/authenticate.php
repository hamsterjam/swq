<?php

if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
include_once ROOT.'php/token.php';

$tokenData;
function authenticate(bool $requireAdmin = false) {
    global $tokenData;

    $token = $_COOKIE['auth'];

    if (!$token) {
        http_response_code(401); // Unauthorized
        exit();
    }

    if (!validateToken($token, $tokenData)) {
        http_response_code(401); // Unauthorized
        exit();
    }

    if ($requireAdmin && !tokenData('admin')) {
        http_response_code(403); // Forbidden
        exit();
    }
}

function &tokenData(string $key) {
    global $tokenData;
    return $tokenData[$key];
}

function reissueToken($newData = []) {
    static $hasIssued = false;
    if ($hasIssued) return;

    global $tokenData;
    foreach ($newData as $key => $value) {
        $tokenData[$key] = $value;
    }
    header('Authorization: Bearer ' . generateToken($tokenData));
    $hasIssued = true;
}

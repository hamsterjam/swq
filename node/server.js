/* jshint node: true */
'use strict';
var fs = require('fs');
var crypto = require('crypto');
var http = require('http');
var WebsocketServer = require('websocket').server;

const keyfile = 'secret/tokensign.key';

var server = http.createServer(function(request, response) {
    function getPostParams(request, callback) {
        if (request.method === 'POST') {
            let body = '';

            request.on('data', (data) => {
                body += data;
            });

            request.on('end', () => {
                var data;
                try {
                    data = JSON.parse(body);
                }
                catch (e) {
                    response.writeHead(400);
                    response.end();
                    return;
                }
                callback(data);
            });
        }
    }

    if (request.method === 'POST') {
        getPostParams(request, function(data) {
            messageClients(data);
            response.writeHead(200);
            response.end();
        });
    }
});
server.listen(8080);

var clients = [];
var websocketServer = new WebsocketServer({
    httpServer: server,
});

websocketServer.on('request', (request) => {
    request.accept(null, request.origin);
});

websocketServer.on('connect', (connection) => {
    connection.expires = 0;
    connection.on('message', (message) => {
        handleMessage(JSON.parse(message.utf8Data), connection);
    });
    clients.push(connection);
});

function handleMessage(message, connection) {
    console.log('[IN]  ' + formatMessage(message));

    switch (message.type) {
        case 'auth':
            let valid, tokenData;
            [valid, tokenData] = validateToken(message.data);
            if (valid) {
                console.log('\tUser "' + tokenData.user + '" timeout set to "' + formatTime(tokenData.till) + '"');
                connection.expires = tokenData.till;
            }
            break;
    }
}

function validateToken(token) {
    let key = fs.readFileSync(keyfile);
    let hmac = crypto.createHmac('sha256', key);
    let now = Math.floor(Date.now() / 1000);

    let data, signature;
    [data, signature] = token.split('.');

    hmac.update(data);
    let expected = hmac.digest('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/\=/g, '');

    if (expected !== signature) return [false, null,];

    let dataOut = JSON.parse(Buffer.from(data
        .replace(/-/g, '+')
        .replace(/_/g, '/'),
    'base64').toString());

    if (dataOut.till < now) return [false, null,];

    return [true, dataOut,];
}

function messageClients(message) {
    clients = clients.filter(item => item.connected);

    console.log('[OUT] ' + formatMessage(message));

    let now = message.time;
    let payload = JSON.stringify(message);
    for (var client of clients) {
        if (client.expires < now ) continue;
        client.sendUTF(payload);
    }
}

function formatMessage(message) {
    let time = formatTime(message.time);
    let data = message.data;

    switch (message.type) {
        case 'msg':
            return 'Message: [' + time + '] ' + data.user + ': ' + data.msg;
        case 'auth':
            return 'Received authorization token:';
    }
}

function formatTime(timestamp) {
    let dateObj = new Date(timestamp * 1000);

    let year = dateObj.getFullYear().toString().padStart(4, '0');
    let month = dateObj.getMonth().toString().padStart(2, '0');
    let day = dateObj.getDate().toString().padStart(2, '0');
    let hour = dateObj.getHours().toString().padStart(2, '0');
    let minute = dateObj.getMinutes().toString().padStart(2, '0');
    let second = dateObj.getSeconds().toString().padStart(2, '0');

    let date = year + '-' + month + '-' + day;
    let time = hour + ':' + minute + ':' + second;

    return date + ' ' + time;
}

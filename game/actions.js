(function() {
    'use strict';
    let staticActions = [
        {
            name: 'Search',
            action: searchStart,
        },
        null,
        null,
        null,
        {
            name: 'Logout',
            action: logout,
        },
    ];

    let parent = null; // Set on DOMContentLoaded

    document.addEventListener('DOMContentLoaded', () => {
        parent = document.querySelector('#actions');
        if (document.querySelector('#templates > .admin-page') !== null) {
            staticActions.push({
                name: 'Admin',
                action: openAdminPage,
            });
        }
        setActions(staticActions, true);
    });

    function setActions(actions, isStatic = false) {
        clearActions(isStatic);

        // Add new actions
        for (let data of actions) {
            let newNode;
            if (data !== null) {
                newNode = document.createElement('button');
                newNode.innerHTML = data.name;
                newNode.addEventListener('click', data.action);
            }
            else {
                newNode = document.createElement('div');
            }

            if (isStatic) newNode.className = 'static';

            parent.appendChild(newNode);
        }
    }

    function clearActions(isStatic = false) {
        if (isStatic) {
            // Remove all children
            parent.innerHTML = '';
        }
        else {
            // Remove all non-static children
            let children = parent.childNodes;
            for (let i = children.length - 1; i >= 0; --i) {
                let child = children.item(i);
                if (!child.classList.contains('static')) child.remove();
            }
        }

    }

    function logout() {
        document.cookie   = 'auth=;samesite=strict';
        document.location = '/';
    }

    const pickupPage = 'action/pickup.php';

    function searchStart() {
        SWQ.sendPost(pickupPage,
            {
                type: 'search',
            },
            resp => {
                let page = new SWQ.WaitPage('Searching...', resp, searchFinish);
                page.open();
            }
        );
    }


    function searchFinish() {
        let page = new SWQ.PickupPage();
        page.open();
        SWQ.sendPost(pickupPage,
            {
                type: 'searchEnd',
            },
            resp => page.setData(resp)
        );
    }

    function openAdminPage() {
        let page = new SWQ.AdminPage();
        page.open();
    }

    // Export
    SWQ.setActions   = setActions;
    SWQ.clearActions = clearActions;
})();

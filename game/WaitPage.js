(function() {
    'use strict';

    function WaitPage(heading, time, callback) {
        SWQ.Page.call(this, 'wait-page', {requireTop: true,});

        this.time     = time;
        this.callback = callback;
        this.interval = null;
        this.content  = this.page.querySelector(':scope .content');

        this.page.querySelector(':scope .heading').innerHTML = heading;
        this.page.querySelector(':scope button').addEventListener('click', () => {
            this.close();
        });
    }

    WaitPage.prototype = Object.create(SWQ.Page.prototype);

    WaitPage.prototype.setContent = function(content) {
        this.content.innerHTML = content;
    };

    WaitPage.prototype.open = function() {
        SWQ.Page.prototype.open.call(this);

        let targetTime = (Date.now() / 1000) + this.time;
        this.setContent(this.time);

        this.interval = setInterval(() => {
            let now = Date.now() / 1000;
            let remaining = Math.ceil(targetTime - now);

            if( remaining > 0 )
            {
                this.setContent(remaining);
            }
            else {
                this.close();
                this.callback();
            }
        }, 100);
    };

    WaitPage.prototype.close = function() {
        SWQ.Page.prototype.close.call(this);
        if (this.interval) clearInterval(this.interval);
    };

    // Export
    SWQ.WaitPage = WaitPage;
})();

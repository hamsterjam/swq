(function() {
    'use strict';

    function AdminDatabaseView(databaseManipPage, fixedColumns, generateRowActions = () => []) {
        let template = document.querySelector('#templates > .admin-database-view');
        this.node = template.cloneNode(true);

        this.grid = new SWQ.AdminGridView( (data) => generateRowActions(data).concat([
            {
                name: 'Modify',
                action: () => this.modify(data),
            },
        ]));
        this.grid.useNode(this.node.querySelector(':scope > .grid'));

        this.databaseManipPage = databaseManipPage;
        this.fixedColumns = fixedColumns;
        this.colNames = null;
        this.onLoadData = () => {};

        this.node.querySelector(':scope > button.add-new').addEventListener('click', () => this.addNew());

        SWQ.sendPost(this.databaseManipPage,
            {
                type: 'get',
            },
            (resp) => {
                this.onLoadData(resp);
                if (!resp || resp.length === 0) return;
                this.colNames = Object.keys(resp[0]);
                this.grid.configure(Object.keys(resp[0]));
                for (let item of resp) this.grid.append(item);
            }
        );
    }

    AdminDatabaseView.prototype.modify = function(data) {
        new Promise(resolve => {
            let columns = Object.keys(data)
                .filter(item => !this.fixedColumns.includes(item))
                .map(key => ({name: key, value: data[key],}));

            new SWQ.InputPopup('Modify ID: ' + data.id, columns, resolve);
        }).then(ret => new Promise(resolve => {
            ret.id = data.id;
            SWQ.sendPost(this.databaseManipPage,
                {
                    type: 'change',
                    data: ret,
                },
                resolve
            );
        })).then(resp => {
            for (let change of resp) this.grid.updateRowById(change);
        });
    };

    AdminDatabaseView.prototype.addNew = function() {
        if (!this.colNames) return;

        new Promise(resolve => {
            let columns = this.colNames.filter(item => !this.fixedColumns.includes(item));
            new SWQ.InputPopup('Create new', columns, resolve);
        }).then(ret => new Promise(resolve => {
            SWQ.sendPost(this.databaseManipPage,
                {
                    type: 'add',
                    data: ret,
                },
                resolve
            );
        })).then(resp => {
            for (let newItem of resp) this.grid.append(newItem);
        });
    };

    AdminDatabaseView.prototype.useNode = function(node) {
        node.replaceWith(this.node);
    };

    // Export
    SWQ.AdminDatabaseView = AdminDatabaseView;
})();

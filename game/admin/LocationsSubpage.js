(function() {
    'use strict';

    const adminLocationManipPage = 'action/admin/locationManip.php';

    const marginX = 30;
    const marginY = 30;

    function Link(from, to) {
        this[0]  = from;
        this[1]  = to;
    }

    Link.prototype.isSameAs = function(other) {
        if (this[0] === other[0] && this[1] === other[1]) return true;
        if (this[0] === other[1] && this[1] === other[0]) return true;
        return false;
    };

    let linkDirection = Object.freeze({
        north: 0,
        east:  1,
        south: 2,
        west:  3,

        stringify: (direction) => {
            for (let key of Object.keys(linkDirection)) {
                if (linkDirection[key] === direction) return key;
            }
        },
    });

    function AdminLocationsSubpage(adminPage) {
        SWQ.AdminSubpage.call(this, 'admin-locations-subpage', adminPage);
        SWQ.SelectManager.register(this);
    }

    AdminLocationsSubpage.prototype = Object.create(SWQ.AdminSubpage.prototype);

    AdminLocationsSubpage.prototype.generate = function() {
        SWQ.AdminSubpage.prototype.generate.call(this);

        this.dbView = new SWQ.AdminDatabaseView(adminLocationManipPage,
            [
                'id',
                'link_north',
                'link_east',
                'link_south',
                'link_west',
            ]
        );
        this.dbView.useNode(this.node.querySelector(':scope > .grid'));
        this.dbView.onLoadData = data => this.loadData(data);

        // Hook updateRowById so we can update the map as well
        let oldUpdateRowById = this.dbView.grid.updateRowById;
        this.dbView.grid.updateRowById = (item) => {
            oldUpdateRowById.call(this.dbView.grid, item);

            if (item.location !== undefined) {
                let commaIndex = item.location.indexOf(',');
                this.data[item.id].x = +item.location.substring(1, commaIndex);
                this.data[item.id].y = +item.location.substring(commaIndex + 1, item.location.length - 1);
            }

            let links = [
                item.link_north,
                item.link_east,
                item.link_south,
                item.link_west,
            ];

            let notSame = a => b => a !== b;

            for (let i = 0; i < links.length; ++i ) {
                let linkTarget = links[i];
                if (linkTarget === undefined) continue;

                let existingLink = this.data[item.id].links[i];
                let createNew = false;

                if (existingLink === undefined) {
                    if (linkTarget === null) continue;
                    createNew = true;
                }
                else {
                    let existingTarget = (existingLink[0] === item.id) ? existingLink[1] : existingLink[0];
                    if (linkTarget === existingTarget) continue;

                    // The existing link needs to be removed
                    this.links = this.links.filter(notSame(existingLink));

                    this.data[item.id].links[i]              = undefined;
                    this.data[existingTarget].links[(i+2)%4] = undefined;

                    // Then we need to replace the link with a new one
                    if (linkTarget !== null) createNew = true;
                }

                if (createNew) {
                    let newLink = new Link(item.id, linkTarget);
                    this.links.push(newLink);
                    this.data[newLink[0]].links[i]       = newLink;
                    this.data[newLink[1]].links[(i+2)%4] = newLink;
                }
            }

            this.resetDisplay();
        };

        this.map = this.node.querySelector(':scope > .map');
        this.lineContext = this.map.querySelector(':scope > .map-lines').getContext('2d');

        this.data    = {};
        this.links   = [];
        this.buttons = {};

        this.selectedNode = null;
    };

    AdminLocationsSubpage.prototype.setSelection = function(id) {
        this.unsetSelection();

        SWQ.setActions([
            {
                name: 'Link (N)',
                action: () => this.generateLink(id, linkDirection.north),
            }, {
                name: 'Link (E)',
                action: () => this.generateLink(id, linkDirection.east),
            }, {
                name: 'Link (S)',
                action: () => this.generateLink(id, linkDirection.south),
            }, {
                name: 'Link (W)',
                action: () => this.generateLink(id, linkDirection.west),
            },
        ]);

        this.selectedNode = this.buttons[id];
        this.selectedNode.style.border = '2px solid red';
    };

    AdminLocationsSubpage.prototype.unsetSelection = function() {
        if (this.selectedNode !== null) {
            this.selectedNode.style.border = '';
        }

        this.selectedNode = null;

        SWQ.clearActions();
    };

    AdminLocationsSubpage.prototype.generateLink = function(fromId, direction) {
        SWQ.setActions([
            {
                name: 'Unlink',
                action: () => {
                    SWQ.SelectManager.stopIntercept();
                    this.unsetSelection();
                    this.unlink(fromId, direction);
                },
            }, {
                name: 'Cancel',
                action: () => {
                    SWQ.SelectManager.stopIntercept();
                    this.unsetSelection();
                },
            },
        ]);

        new Promise(resolve => {
            SWQ.SelectManager.interceptNext((...args) => resolve(args));
        }).then(([selector, toId]) => new Promise(resolve => {
            this.unsetSelection();
            if (selector !== this) {
                //TODO// A toast would be good here
                return;
            }

            SWQ.sendPost(adminLocationManipPage,
                {
                    type: 'link',
                    data: {
                        from: fromId,
                        to:   toId,
                        dir:  linkDirection.stringify(direction),
                    },
                },
                resolve
            );
        })).then(resp => {
            for (let change of resp) this.dbView.grid.updateRowById(change);
        });
    };

    AdminLocationsSubpage.prototype.unlink = function(id, direction) {
        SWQ.sendPost(adminLocationManipPage,
            {
                type: 'unlink',
                data: {
                    id: id,
                    dir: linkDirection.stringify(direction),
                },
            },
            (resp) => {
                for (let change of resp) this.dbView.grid.updateRowById(change);
            }
        );
    };

    AdminLocationsSubpage.prototype.loadData = function(data) {
        // Save data by id
        for (let item of data) {
            let commaIndex = item.location.indexOf(',');
            this.data[item.id] = {
                x: +item.location.substring(1, commaIndex),
                y: +item.location.substring(commaIndex + 1, item.location.length - 1),
                links: [],
            };
        }

        let compareLinkTo = (compareTo) => {
            return (item) => {
                return item.isSameAs(compareTo);
            };
        };

        // Create list of links
        for (let item of data) {
            let linkIds = [
                item.link_north,
                item.link_east,
                item.link_south,
                item.link_west,
            ];

            for (let i = 0; i < linkIds.length; ++i) {
                let linkId = linkIds[i];
                if (linkId === null) continue;
                let newLink = new Link(item.id, linkId);
                if (!this.links.some(compareLinkTo(newLink))) {
                    this.links.push(newLink);
                    this.data[newLink[0]].links[i]       = newLink;
                    this.data[newLink[1]].links[(i+2)%4] = newLink;
                }
            }
        }

        this.resetDisplay();
    };

    AdminLocationsSubpage.prototype.resetDisplay = function() {
        // Find size limits (minimum set to the map size)
        let maxX = (this.map.clientWidth)  / 2 - marginX;
        let maxY = (this.map.clientHeight) / 2 - marginY;
        for (let id in this.data) {
            let item = this.data[id];
            if (Math.abs(item.x) > maxX) maxX = Math.abs(item.x);
            if (Math.abs(item.y) > maxY) maxY = Math.abs(item.y);
        }

        // So we are contained in a +- maxX, maxY box, therefore the distance
        // to the center is just maxX, maxY (+ a margin)
        this.originX = maxX + marginX;
        this.originY = maxY + marginY;

        // Origin is always in the center, so we multiply by 2 to get the
        // width and height
        this.lineContext.canvas.width  = this.originX * 2;
        this.lineContext.canvas.height = this.originY * 2;
        this.lineContext.clearRect(0, 0, this.lineContext.canvas.width, this.lineContext.canvas.height);

        // The offset we need to scroll is just the difference
        let offsetX = this.originX - (this.map.clientWidth / 2);
        let offsetY = this.originY - (this.map.clientHeight / 2);
        this.map.scrollTo(offsetX, offsetY);

        // Draw each link
        this.lineContext.lineWidth = 5;
        this.links.forEach(this.drawLink.bind(this));

        // Create buttons
        while (this.map.children.length > 1) this.map.lastChild.remove();
        for (let id in this.data) this.map.appendChild(this.createMapButton(id));
    };

    AdminLocationsSubpage.prototype.drawLink = function(link) {
        let point1 = this.data[link[0]];
        let point2 = this.data[link[1]];

        let ctx = this.lineContext;
        ctx.beginPath();
        ctx.moveTo(point1.x + this.originX, -point1.y + this.originY);
        ctx.lineTo(point2.x + this.originX, -point2.y + this.originY);
        ctx.stroke();
    };

    AdminLocationsSubpage.prototype.createMapButton = function(id) {
        let newButton;
        if (this.buttons[id] !== undefined) {
            newButton = this.buttons[id];
        }
        else {
            newButton = document.createElement('button');
            newButton.innerHTML = id;
            newButton.addEventListener('click', () => this.setSelection(id));
            this.buttons[id] = newButton;
        }

        let item = this.data[id];
        newButton.style.left =  item.x + this.originX;
        newButton.style.top  = -item.y + this.originY;

        return newButton;
    };

    // Export
    SWQ.AdminLocationsSubpage = AdminLocationsSubpage;
})();

(function() {
    'use strict';

    let selectType = Object.freeze({
        row:    0,
        column: 1,
    });

    let sortDirection = Object.freeze({
        ascending:   1,
        descending: -1,
    });

    function AdminGridView(generateRowActions) {
        let template = document.querySelector('#templates > .admin-grid-view');
        this.node = template.cloneNode(true);

        this.rows = [];
        this.columns = [];
        this.hiddenColumns = [];

        this.rowNodes = [];
        this.columnNodes = {};

        this.generateRowActions = generateRowActions;

        this.selectedNode = null;

        SWQ.SelectManager.register(this);
    }

    AdminGridView.prototype.setSelection = function(type, data) {
        this.unsetSelection();

        let node = null;
        switch (type) {
            case (selectType.row): {
                node = this.rowNodes[data][0];
                let rowData = this.rows.find(item => item.position === data).data;
                SWQ.setActions(this.generateRowActions(rowData));
                break;
            }

            case (selectType.column): {
                node = this.columnNodes[data];
                SWQ.setActions([
                    {
                        name: 'Move →',
                        action: () => this.moveColumn(data, +1),
                    }, {
                        name: 'Move ←',
                        action: () => this.moveColumn(data, -1),
                    }, {
                        name: 'Sort ↓',
                        action: () => this.sortBy(data, sortDirection.ascending),
                    }, {
                        name: 'Sort ↑',
                        action: () => this.sortBy(data, sortDirection.descending),
                    }, {
                        name: 'Hide',
                        action: () => {
                            this.hideColumn(data);
                            this.unsetSelection();
                        },
                    },
                ]);
                break;
            }
        }

        this.selectedNode = node;
        node.style.margin = '-2px';
        node.style.border = '2px solid red';
    };

    AdminGridView.prototype.unsetSelection = function() {
        if (this.selectedNode !== null) {
            this.selectedNode.style.margin = '';
            this.selectedNode.style.border = '';
        }

        this.selectedNode = null;

        SWQ.clearActions();
    };

    AdminGridView.prototype.moveColumn = function(col, offset) {
        let origIndex = this.columns.indexOf(col);
        let destIndex = origIndex + offset;

        if (origIndex < 0 || offset === 0) return;
        if (destIndex < 0 || destIndex >= this.columns.length) return;

        let styleString = this.node.style.gridTemplateColumns;

        // Store the rightmost index first
        let indicies = [
            (origIndex < destIndex) ? destIndex : origIndex,
            (origIndex < destIndex) ? origIndex : destIndex,
        ];
        let colNames = indicies.map(index => '[' + this.columns[index] + ']');
        for (let i = 0; i < 2; ++i) styleString = styleString.replace(colNames[i], colNames[(i+1)%2]);

        this.node.style.gridTemplateColumns = styleString;

        this.columns[origIndex] = this.columns[destIndex];
        this.columns[destIndex] = col;
    };

    AdminGridView.prototype.hideColumn = function(col) {
        let colIndex = this.columns.indexOf(col);
        if (colIndex < 0) return;

        let styleString = this.node.style.gridTemplateColumns;

        let words = styleString.split(' ');
        let index = words.indexOf('[' + col + ']');
        if (index < 0 || index + 1 >= words.length) return;
        words.splice(index, 2);
        this.node.style.gridTemplateColumns = words.join(' ');

        for (let child = this.node.firstChild; child; child = child.nextSibling) {
            if (child.style.gridColumn.startsWith(col)) {
                child.style.display = 'none';
            }
        }

        this.columns.splice(colIndex, 1);
        this.hiddenColumns.push(col);
    };

    AdminGridView.prototype.sortBy = function(col, sortdir) {
        this.rows.sort((lhs, rhs) => {
            let lhsVal = lhs.data[col];
            let rhsVal = rhs.data[col];

            if (lhsVal === undefined && rhsVal === undefined) return 0;
            if (lhsVal === undefined) return -1 * sortdir;
            if (rhsVal === undefined) return  1 * sortdir;
            if (typeof lhsVal !== typeof rhsVal) return 0;

            switch (typeof lhsVal) {
                case 'string':
                    lhsVal = lhsVal.toLowerCase();
                    rhsVal = rhsVal.toLowerCase();
                    if (lhsVal < rhsVal) return -1 * sortdir;
                    if (lhsVal > rhsVal) return  1 * sortdir;
                    return 0;

                case 'boolean':
                    if ( lhsVal && !rhsVal) return -1 * sortdir;
                    if (!lhsVal &&  rhsVal) return  1 * sortdir;
                    return 0;

                default:
                    return (lhsVal - rhsVal) * sortdir;
            }
        });

        for (let i = 0; i < this.rows.length; ++i) {
            let row = this.rows[i];
            for (let node of this.rowNodes[row.position]) {
                node.style.gridRow = '' + (i + 2) + ' / span 1';
            }
        }
    };

    AdminGridView.prototype.useNode = function(node) {
        node.replaceWith(this.node);
    };

    AdminGridView.prototype.append = function(item) {
        let row = [];

        let position = this.rows.length;
        let selector = document.createElement('div');
        let selectButton = document.createElement('button');
        selectButton.addEventListener('click', () => {
            this.setSelection(selectType.row, position);
        });
        selector.appendChild(selectButton);
        this.node.appendChild(selector);
        row.push(selector);

        for (let key in item) {
            let value = item[key];
            let newDiv = document.createElement('div');
            newDiv.style.gridColumn = '' + key + ' / span 1';
            newDiv.innerHTML = value;

            if (this.hiddenColumns.includes(key)) {
                newDiv.style.display = 'none';
            }

            this.node.appendChild(newDiv);
            row.push(newDiv);
        }

        this.rows.push({
            position: position,
            data: item,
        });
        this.rowNodes.push(row);
    };

    AdminGridView.prototype.configure = function(headers) {
        this.node.innerHTML = '';
        this.rows = [];
        this.columns = headers;

        let empty = document.createElement('div');
        empty.className = 'grid-cell';
        this.node.appendChild(empty);

        let makeCallback = (header) => {
            return () => this.setSelection(selectType.column, header);
        };

        let template = 'auto';
        for (let header of headers) {
            template += ' [' + header + '] auto';

            let headerButton = document.createElement('button');
            let headerDiv = document.createElement('div');
            headerDiv.appendChild(headerButton);

            headerDiv.style.gridColumn = '' + header + ' / span 1';
            headerButton.innerHTML = header;
            headerButton.addEventListener('click', makeCallback(header));

            this.node.appendChild(headerDiv);
            this.columnNodes[header] = headerDiv;
        }
        this.node.style.gridTemplateColumns = template;
    };

    AdminGridView.prototype.updateRowById = function(data) {
        let row = this.rows.find(item => item.data.id === data.id);

        // Loop over data because thats the order we are expecting
        let index = 1;
        for (let key in row.data) {
            let node = this.rowNodes[row.position][index++];
            if (key === 'id') continue;
            if (data[key] === undefined) continue;
            node.innerHTML = data[key];
            row.data[key]  = data[key];
        }
    };

    // Export
    SWQ.AdminGridView = AdminGridView;
})();

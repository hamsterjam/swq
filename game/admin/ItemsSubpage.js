(function() {
    'use strict';

    const adminItemManipPage = 'action/admin/itemManip.php';

    function AdminItemsSubpage(adminPage) {
        SWQ.AdminSubpage.call(this, 'admin-items-subpage', adminPage);
    }

    AdminItemsSubpage.prototype = Object.create(SWQ.AdminSubpage.prototype);

    AdminItemsSubpage.prototype.generate = function() {
        SWQ.AdminSubpage.prototype.generate.call(this);

        this.dbView = new SWQ.AdminDatabaseView(adminItemManipPage,
            [
                'id',
            ],
            data =>
            [
                {
                    name: 'Take 1',
                    action: () => this.take(data, 1),
                }, {
                    name: 'Take stack',
                    action: () => this.take(data, data.stack_limit),
                }, {
                    name: 'Take X',
                    action: () => this.take(data),
                },
            ]
        );
        this.dbView.useNode(this.node.querySelector(':scope > .grid'));
    };

    AdminItemsSubpage.prototype.take = function(data, count) {
        new Promise(resolve => {
            if (count !== undefined) {
                resolve({count: count,});
                return;
            }
            new SWQ.InputPopup('Take how many?', ['count',], resolve);
        }).then(({count: count,}) => new Promise(resolve => {
            count = +count;
            if (isNaN(count) || count <= 0) {
                count = 1;
            }
            if (count > data.stack_limit) {
                count = data.stack_limit;
            }

            SWQ.sendPost(adminItemManipPage,
                {
                    type: 'take',
                    data: {
                        'item': data.id,
                        'count': count,
                    },
                },
                resolve
            );
        })).then(resp => {
            let inventory = SWQ.InventoryPanel.instance;
            if (inventory !== null) {
                inventory.processPickupData(resp, data.name);
            }
        });
    };

    // Export
    SWQ.AdminItemsSubpage = AdminItemsSubpage;
})();

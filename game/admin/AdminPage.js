(function() {
    'use strict';

    const revokeAdminPage = 'action/admin/revokeAdmin.php';

    function AdminPage() {
        SWQ.Page.call(this, 'admin-page');

        this.sidebarNode = this.page.querySelector(':scope > .sidebar');
        this.contentNode = this.page.querySelector(':scope > .content');

        this.currentSubpage = null;

        let adminActions = [];

        // Has to be here so subpages can be included after this file
        let adminSubpages = [
            {
                name: 'Items',
                class: SWQ.AdminItemsSubpage,
            }, {
                name: 'Locations',
                class: SWQ.AdminLocationsSubpage,
            },
        ];

        for (let data of adminSubpages) {
            let page = new data.class(this);
            adminActions.push({
                name: data.name,
                action: page.open.bind(page),
            });
        }

        // Has to be here for `this` to point to the right thing
        adminActions = adminActions.concat([
            {
                name: 'Hide Admin',
                action: () => SWQ.sendPost(revokeAdminPage, null, () => location.reload()),
            }, {
                name: 'Close',
                action: () => this.close(),
            },
        ]);

        for (let data of adminActions) {
            let newButton = document.createElement('button');
            newButton.innerHTML = data.name;
            newButton.addEventListener('click', data.action);

            this.sidebarNode.appendChild(newButton);
        }
    }

    AdminPage.prototype = Object.create(SWQ.Page.prototype);

    function AdminSubpage(templateClass, adminPage) {
        this.templateClass = templateClass;
        this.adminPage = adminPage;

        this.node = null;
        if (this.adminPage.currentSubpage === null) {
            this.open();
        }
    }

    AdminSubpage.prototype.open = function() {
        if (AdminSubpage.current === this) return;
        if (this.node === null) this.generate();

        if (this.adminPage.currentSubpage !== null) {
            this.adminPage.currentSubpage.node.style.display = 'none';
        }
        this.adminPage.currentSubpage = this;
        this.node.style.display = '';
    };

    AdminSubpage.prototype.generate = function() {
        let template = document.querySelector('#templates > .' + this.templateClass);
        this.node = template.cloneNode(true);
        this.adminPage.contentNode.appendChild(this.node);
    };

    // Export
    SWQ.AdminPage = AdminPage;
    SWQ.AdminSubpage = AdminSubpage;
})();

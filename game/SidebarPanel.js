(function() {
    'use strict';

    function SidebarPanel(panel) {
        this.node = panel;
        this.node.style.display = 'none';
    }

    SidebarPanel.current = null;
    SidebarPanel.parent  = null; // Set at DOMContentLoaded

    SidebarPanel.prototype.close = function() {
        this.node.style.display = 'none';
        if (SidebarPanel.current === this) SidebarPanel.current = null;
    };

    SidebarPanel.prototype.open = function() {
        if (SidebarPanel.current !== null) SidebarPanel.current.close();
        this.node.style.display = '';
        SidebarPanel.current = this;
    };

    document.addEventListener('DOMContentLoaded', () => {
        // define this here so they can be included after this file
        let panelClasses = [
            SWQ.InventoryPanel,
            SidebarPanel,
            SidebarPanel,
            SidebarPanel,
        ];
        let tabs   = document.querySelectorAll('#sidebar > .tabs > div');
        let panels = document.querySelectorAll('#sidebar > .panels > div');

        for (let i = 0; i < tabs.length; ++i) {
            let panel = new panelClasses[i](panels[i]);
            tabs[i].addEventListener('click', panel.open.bind(panel));
        }
    });

    // Export
    SWQ.SidebarPanel = SidebarPanel;
})();

(function() {
    'use strict';

    function InputPopup(title, colData, callback) {
        let template = document.querySelector('#templates > .input-popup');
        let parent   = document.querySelector('.aspect-box');
        this.node = template.cloneNode(true);

        parent.appendChild(this.node);

        this.grid = this.node.querySelector(':scope .grid');

        this.node.querySelector(':scope .cancel').addEventListener('click', () => {
            this.node.remove();
        });

        this.node.querySelector(':scope .send').addEventListener('click', () => {
            let data = {};
            let inputs = this.grid.querySelectorAll(':scope > input');
            for (let input of inputs) {
                data[input.name] = input.value;
            }

            this.node.remove();
            callback(data);
        });

        this.node.querySelector(':scope .heading').innerHTML = title;
        this.initGrid(colData);

        this.grid.querySelector(':scope input').focus();
    }

    InputPopup.prototype.initGrid = function(colData) {
        for (let column of colData) {
            let colName  = '';
            let colValue = '';
            if (typeof column === 'object') {
                colName  = column.name;
                colValue = column.value;
            }
            else {
                colName = column.toString();
            }

            let heading = document.createElement('div');
            heading.innerHTML = colName;

            let input = document.createElement('input');
            input.name = colName;
            input.value = colValue;

            this.grid.appendChild(heading);
            this.grid.appendChild(input);
        }
    };

    // Export
    SWQ.InputPopup = InputPopup;
})();

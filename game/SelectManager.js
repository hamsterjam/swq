(function() {
    'use strict';

    function SelectManager() {
        this.current = null;
        this.intercept = null;
    }

    SelectManager.prototype.register = function(item) {
        // Hook the setSelection function
        let oldSetSelection = item.setSelection;
        item.setSelection = ((...args) => {
            if (this.intercept !== null) {
                this.intercept(item, ...args);
                this.intercept = null;
                return;
            }

            if (this.current !== null && this.current !== item) {
                this.current.unsetSelection();
                this.current = item;
            }
            this.current = item;
            oldSetSelection.call(item, ...args);
        }).bind(this);
    };

    SelectManager.prototype.unsetSelection = function() {
        if (this.current === null) return;
        this.current.unsetSelection();
        this.current = null;
    };

    SelectManager.prototype.interceptNext = function(callback) {
        this.intercept = callback;
    };

    SelectManager.prototype.stopIntercept = function() {
        this.intercept = null;
    };

    // Export
    SWQ.SelectManager = new SelectManager();
})();

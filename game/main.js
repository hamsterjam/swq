var SWQ = {};
(function() {
    'use strict';
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    var connection = new WebSocket('ws://' + window.location.hostname + ':8080');
    connection.addEventListener('open', () => {
        sendAuth();
    });

    document.addEventListener('DOMContentLoaded', () => {
        for (let image of document.querySelectorAll('img')) {
            image.draggable = false;
        }

        Page.container  = document.querySelector('#main');
    });

    //
    // Network Management
    //

    function sendAuth() {
        let authCookie = document.cookie.split(';').find(item => item.startsWith('auth='));
        let token = authCookie.split('=')[1];

        connection.send(JSON.stringify({
            type: 'auth',
            data: token,
        }));
    }

    function sendPost(page, data, onDone = undefined, onError = undefined) {
        let xhr = new XMLHttpRequest();
        xhr.addEventListener('loadend', () => {
            let resp = null;
            try {
                resp = JSON.parse(xhr.responseText);
            }
            catch (e) {
                // Just set to null, it's fine
                resp = null;
            }

            switch (xhr.status) {
                case 200: {
                    let token = xhr.getResponseHeader('Authorization').split(' ')[1];
                    document.cookie = 'auth=' + token + ';samesite=strict';
                    sendAuth();

                    if (onDone !== undefined) onDone(resp);
                    break;
                }
                case 401: {
                    alert('Timed out!');
                    document.location = '/';
                    break;
                }
                default: {
                    if (onError !== undefined) onError(resp, xhr.Status);
                    break;
                }
            }
        });
        xhr.open('POST', page);
        xhr.setRequestHeader('Content-Type', 'application/json');

        if (data !== null && data !== undefined) {
            xhr.send(JSON.stringify(data));
        }
        else {
            xhr.send();
        }
    }

    //
    // Page Management
    //

    const STATUS_TIME = 5000;

    function Page(templateClass, opt = {}) {
        this.requireTop = (opt.requireTop !== undefined) ? opt.requireTop : false;

        let template = document.querySelector('#templates > .' + templateClass);

        this.page = template.cloneNode(true);
        this.page.style.display = 'none';
        Page.container.appendChild(this.page);
    }

    Page.stack = [];
    Page.container  = null; // Set on DOMContentLoaded
    Page.clearStatusTimeout = null;

    Page.prototype.open = function() {
        // Only allow one of each page (requireTop does this by closing old pages)
        if (!this.requireTop) {
            let existing = Page.stack.find(
                item => (item !== this && Object.getPrototypeOf(item) === Object.getPrototypeOf(this))
            );
            if (existing !== undefined) {
                existing.open();
                return;
            }
        }

        this.page.style.display = '';

        if (Page.clearStatusTimeout !== null) {
            clearTimeout(Page.clearStatusTimeout);
        }
        Page.stack = Page.stack.filter(item => item !== this);
        Page.stack.push(this);

        if (Page.stack.length > 1) {
            let prev = Page.stack[Page.stack.length - 2];
            prev.page.style.display = 'none';
            if( prev.requireTop ) prev.close();
        }
    };

    Page.prototype.close = function() {
        let isTop = (Page.stack[Page.stack.length - 1] === this);

        this.page.style.display = 'none';
        this.page.remove();
        Page.stack = Page.stack.filter(item => item !== this);

        if (!isTop) return;

        let prev = Page.stack[Page.stack.length - 1];
        if (prev) {
            prev.open();
        }
    };

    // Export
    SWQ.connection = connection;
    SWQ.sendPost   = sendPost;
    SWQ.Page       = Page;
})();

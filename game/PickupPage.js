(function() {
    'use strict';

    const pickupPage = 'action/pickup.php';

    function PickupPage() {
        SWQ.Page.call(this, 'pickup-page', {requireTop: true,});

        this.page.querySelector(':scope button').addEventListener('click', this.close.bind(this));
        this.grid = null;
    }

    PickupPage.prototype = Object.create(SWQ.Page.prototype);

    PickupPage.prototype.setData = function(data) {
        for (let i = 0; i < data.length; ++i) data.pos = i;
        let gridNode = this.page.querySelector(':scope .grid');
        gridNode.innerHTML = '';

        this.grid = new SWQ.InventoryGrid(gridNode, data);
        this.grid.onSelect = (pos) => SWQ.setActions([
            {
                name: 'Pickup',
                action: () => this.pickup(pos),
            },
        ]);
    };

    PickupPage.prototype.pickup = function(pos) {
        let data = this.grid.icons[pos].data;
        if (data === undefined) return;

        SWQ.sendPost(pickupPage,
            {
                type: 'pickup',
                data: data.id,
            },
            (resp) => {
                this.grid.remove(pos);

                let inventory = SWQ.InventoryPanel.instance;
                if (inventory !== null) {
                    inventory.processPickupData(resp, data.name);
                }

                if (!resp.some(item => item.type === 'pickup')) {
                    //TODO// A toast would be nice here
                }
            }
        );
    };

    // Export
    SWQ.PickupPage = PickupPage;
})();

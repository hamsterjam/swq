(function() {
    'use strict';

    let mapNode = null;
    let display = null;

    document.addEventListener('DOMContentLoaded', () => {
        mapNode = document.querySelector('#map');
        display = mapNode.querySelector(':scope > .temp-display');
    });
})();

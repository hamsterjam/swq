(function() {
    'use strict';

    const getInventoryPage = 'action/getInventory.php';
    const inventoryManipPage = 'action/inventoryManip.php';

    function InventoryGrid(grid, itemData, maxSize = itemData.length) {
        this.icons      = [];
        this.grid       = grid;
        this.selection  = -1;
        this.onSelect   = null;
        this.onDeselect = SWQ.clearActions;

        let currPos = 0;
        for (let item of itemData) {
            for (let i = currPos; i < item.pos; ++i) this.append(new SWQ.PlaceholderIcon());
            this.append(new SWQ.ItemIcon(item));
            currPos = item.pos + 1;
        }

        for (let i = currPos; i < maxSize; ++i) this.append(new SWQ.PlaceholderIcon());

        SWQ.SelectManager.register(this);
    }

    InventoryGrid.prototype.append = function(icon) {
        let pos = this.icons.length;
        this.icons.push(icon);
        this.grid.appendChild(icon.node);

        icon.associate(this, pos);
    };

    InventoryGrid.prototype.replace = function(pos, icon) {
        let old = this.icons[pos];

        if (old === undefined) {
            // Append new placeholders then just append the icon
            for (let i = this.icons.length; i < pos; ++i) this.append(new SWQ.PlaceholderIcon());
            this.append(icon);
            return;
        }

        old.node.replaceWith(icon.node);
        this.icons[pos] = icon;

        old.dissociate();
        icon.associate(this, pos);
    };

    InventoryGrid.prototype.remove = function(pos) {
        if (this.selection === pos) this.unsetSelection();
        this.replace(pos, new SWQ.PlaceholderIcon());
    };

    InventoryGrid.prototype.updateCount = function(pos, count) {
        let item = this.icons[pos];
        item.setCount(count);
    };

    InventoryGrid.prototype.setSelection = function(pos) {
        this.unsetSelection();

        if (pos < 0 || pos >= this.icons.length) return;

        let icon = this.icons[pos];
        if (icon.data === undefined) return;

        let node = icon.node;
        node.style.border = 'solid 2px red';
        node.style.margin = '-2px';
        this.selection = pos;

        if (this.onSelect !== null) this.onSelect(pos);
    };

    InventoryGrid.prototype.unsetSelection = function() {
        if (this.selection < 0) return;
        let node = this.icons[this.selection].node;
        node.style.border = '';
        node.style.margin = '';
        this.selection = -1;

        if (this.onDeselect !== null) this.onDeselect();
    };

    function InventoryPanel(panel) {
        SWQ.SidebarPanel.call(this, panel);
        this.gridNode = this.node.querySelector(':scope .inventory');
        this.grid = null;

        InventoryPanel.instance = this;
    }

    InventoryPanel.prototype = Object.create(SWQ.SidebarPanel.prototype);

    InventoryPanel.instance = null;

    InventoryPanel.prototype.generateGrid = function(callback) {
        SWQ.sendPost(getInventoryPage, null, (resp) => {
            this.grid = new InventoryGrid(this.gridNode, resp);

            this.grid.onSelect = (pos) => SWQ.setActions([
                {
                    name: 'Drop',
                    action: () => this.drop(pos),
                },
            ]);

            callback();
        });
    };

    InventoryPanel.prototype.drop = function(pos) {
        if (this.grid === null) return;

        let data = this.grid.icons[pos].data;
        if (data === undefined) return;

        this.grid.remove(pos);
        SWQ.sendPost(inventoryManipPage, {
            type: 'drop',
            data: data.id,
        });
    };

    InventoryPanel.prototype.processPickupData = function(data, name) {
        for (let change of data) {
            if (this.grid === null) return;
            switch (change.type) {
                case 'insert':
                    this.grid.replace(change.pos, new SWQ.ItemIcon({
                        id: change.id,
                        name: name,
                        count: change.count,
                    }));
                    break;

                case 'change':
                    let icon = this.grid.icons[change.pos];
                    if (icon !== undefined) icon.setCount(change.count);
                    break;
            }
        }
    };

    InventoryPanel.prototype.open = function() {
        let superOpen = SWQ.SidebarPanel.prototype.open.bind(this);
        if (this.grid === null) this.generateGrid(superOpen);
        else superOpen();
    };

    // Export
    SWQ.InventoryGrid  = InventoryGrid;
    SWQ.InventoryPanel = InventoryPanel;
})();

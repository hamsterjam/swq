(function() {
    'use strict';

    function InventoryIcon(templateClass) {
        let template = document.querySelector('#templates > .' + templateClass);

        this.node  = template.cloneNode(true);
        this.panel = null;

        this.grid = null;
        this.pos  = -1;
        this.node.addEventListener('click', () => {
            if (this.grid !== null && this.pos >= 0) {
                this.grid.setSelection(this.pos);
            }
        });
    }

    InventoryIcon.prototype.associate = function(grid, pos) {
        this.grid = grid;
        this.pos  = pos;
    };

    InventoryIcon.prototype.dissociate = function() {
        this.associate(null, -1);
    };

    InventoryIcon.prototype.setCount = function() {};

    function ItemIcon(data) {
        InventoryIcon.call(this, 'item-icon');

        this.data = data;
        this.count = this.node.querySelector(':scope .count');

        this.setCount(data.count);
    }

    ItemIcon.prototype = Object.create(InventoryIcon.prototype);

    ItemIcon.prototype.setCount = function(count) {
        if (count > 1) {
            this.count.innerHTML = '(' + count + ')';
        }
        else {
            this.count.innerHTML = '';
        }
    };

    function PlaceholderIcon() {
        InventoryIcon.call(this, 'placeholder-icon');
    }

    PlaceholderIcon.prototype = Object.create(InventoryIcon.prototype);

    // Export
    SWQ.ItemIcon        = ItemIcon;
    SWQ.PlaceholderIcon = PlaceholderIcon;
})();

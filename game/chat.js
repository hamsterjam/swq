(function() {
    'use strict';
    const sendChatPage = 'action/sendChat.php';

    let chatNode;
    let input;
    let output;

    document.addEventListener('DOMContentLoaded', () => {
        chatNode = document.querySelector('#chat');
        input  = chatNode.querySelector(':scope .input');
        output = chatNode.querySelector(':scope .output');

        chatNode.querySelector(':scope .send-button').addEventListener('click', transmit);
        input.addEventListener('keyup', event => {
            if (event.keyCode === 13) transmit(); // Enter pressed
        });
    });

    SWQ.connection.addEventListener('message', (event) => {
        let message = JSON.parse(event.data);
        let data = message.data;
        switch (message.type) {
            case 'msg':
                addMessage('[' + data.user + ']: ' + data.msg);
        }
    });

    function transmit() {
        SWQ.sendPost(sendChatPage, {
            msg: input.value,
        });

        input.value = '';
    }

    function addMessage(message) {
        let atBottom = (output.scrollTop === output.scrollHeight - output.clientHeight);

        let newDiv = document.createElement('DIV');
        newDiv.innerHTML = message;
        output.appendChild(newDiv);

        if (atBottom) {
            output.scrollTop = output.scrollHeight - output.clientHeight;
        }
    }
})();

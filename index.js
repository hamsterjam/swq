(function() {
    'use strict';
    const loginPage    = 'action/login.php';
    const registerPage = 'action/register.php';
    const redirect     = 'game.php';

    document.addEventListener('DOMContentLoaded', () => {
        let isLogin = true;

        // Setup radio buttons
        let radioEvent = (event) => {
            isLogin = (event.target.value === 'login');
            let registerNodes = document.querySelectorAll('.register');
            for (let node of registerNodes) {
                node.style.display = (isLogin) ? '' : 'block';
            }
            statusMessage();
        };
        for (const radio of document.querySelectorAll('input[name="method"]')) {
            radio.addEventListener('change', radioEvent);

            if (radio.value === 'login') {
                radio.checked = true;
            }
        }

        // Setup button
        document.querySelector('#go-button').addEventListener('click', () => {
            if (isLogin) {
                postLogin();
            }
            else {
                postRegister();
            }
        });
    });

    function postLogin() {
        let username = document.querySelector('#username-input').value;
        let password = document.querySelector('#password-input').value;

        let xhr = new XMLHttpRequest();
        xhr.addEventListener('loadend', () => {
            switch (xhr.status) {
                case 200: // OK
                    let token = xhr.getResponseHeader('Authorization').split(' ')[1];
                    document.cookie   = 'auth=' + token + ';samesite=strict';
                    document.location = redirect;
                    break;
                case 401: // Unauthorized
                    statusMessage('Username and/or password was incorrect!');
                    break;
                default:
                    alert('An error has occured.');
                    break;
            }
        });
        xhr.open('POST', loginPage);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(JSON.stringify({
            user: username,
            pass: password,
        }));
    }

    function postRegister() {
        let username = document.querySelector('#username-input').value;
        let password = document.querySelector('#password-input').value;
        let confirm  = document.querySelector('#confirm-input' ).value;

        if( password !== confirm ) {
            statusMessage('Password and Confirm must match!');
            return;
        }

        let xhr = new XMLHttpRequest();
        xhr.addEventListener('loadend', () => {
            switch (xhr.status) {
                case 200: // OK
                    let token = xhr.getResponseHeader('Authorization').split(' ')[1];
                    document.cookie   = 'auth=' + token + ';samesite=strict';
                    document.location = redirect;
                    break;
                case 409: // Conflict
                    statusMessage('Username already exists');
                    break;
                default:
                    alert('An error has occured.');
                    break;
            }
        });
        xhr.open('POST', registerPage);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(JSON.stringify({
            user: username,
            pass: password,
        }));
    }

    function statusMessage(message) {
        let status = document.querySelector('#status');

        status.style.visibility = (message) ? 'visible' : 'hidden';
        status.innerText = message;
    }
})();

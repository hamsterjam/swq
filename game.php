<?php
if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
include_once ROOT.'php/authenticate.php';
authenticate();
?>
<html>
    <head>
        <title>Sword Wizard Quest!</title>
        <style>
            <?php echo file_get_contents(ROOT.'game.css'); ?>
            <?php echo file_get_contents(ROOT.'template/main/map.css'); ?>
            <?php echo file_get_contents(ROOT.'template/main/sidebar.css'); ?>
            <?php echo file_get_contents(ROOT.'template/main/chat.css'); ?>
            <?php echo file_get_contents(ROOT.'template/inputPopup.css'); ?>
            <?php echo file_get_contents(ROOT.'template/itemIcon.css'); ?>
            <?php echo file_get_contents(ROOT.'template/placeholderIcon.css'); ?>
            <?php echo file_get_contents(ROOT.'template/waitPage.css'); ?>
            <?php echo file_get_contents(ROOT.'template/pickupPage.css'); ?>
            <?php echo file_get_contents(ROOT.'template/inventoryPanel.css'); ?>
            <?php if (tokenData('admin')): ?>
                <?php echo file_get_contents(ROOT.'template/admin/adminPage.css'); ?>
                <?php echo file_get_contents(ROOT.'template/admin/gridView.css'); ?>
                <?php echo file_get_contents(ROOT.'template/admin/databaseView.css'); ?>
                <?php echo file_get_contents(ROOT.'template/admin/itemsSubpage.css'); ?>
                <?php echo file_get_contents(ROOT.'template/admin/locationsSubpage.css'); ?>
            <?php endif; ?>
        </style>
        <script type="text/javascript" src="game/main.js"></script>
        <script type="text/javascript" src="game/InputPopup.js"></script>
        <script type="text/javascript" src="game/SelectManager.js"></script>
        <script type="text/javascript" src="game/WaitPage.js"></script>
        <script type="text/javascript" src="game/actions.js"></script>
        <script type="text/javascript" src="game/chat.js"></script>
        <script type="text/javascript" src="game/map.js"></script>
        <script type="text/javascript" src="game/ItemIcon.js"></script>
        <script type="text/javascript" src="game/SidebarPanel.js"></script>
        <script type="text/javascript" src="game/InventoryPanel.js"></script>
        <script type="text/javascript" src="game/PickupPage.js"></script>
        <?php if (tokenData('admin')): ?>
            <script type="text/javascript" src="game/admin/AdminPage.js"></script>
            <script type="text/javascript" src="game/admin/GridView.js"></script>
            <script type="text/javascript" src="game/admin/DatabaseView.js"></script>
            <script type="text/javascript" src="game/admin/ItemsSubpage.js"></script>
            <script type="text/javascript" src="game/admin/LocationsSubpage.js"></script>
        <?php endif; ?>
    </head>
    <body>
        <div id="templates" style="display:none">
            <?php echo file_get_contents(ROOT.'template/inputPopup.html'); ?>
            <?php echo file_get_contents(ROOT.'template/itemIcon.html'); ?>
            <?php echo file_get_contents(ROOT.'template/placeholderIcon.html'); ?>
            <?php echo file_get_contents(ROOT.'template/waitPage.html'); ?>
            <?php echo file_get_contents(ROOT.'template/pickupPage.html'); ?>
            <?php if (tokenData('admin')): ?>
                <?php echo file_get_contents(ROOT.'template/admin/adminPage.html'); ?>
                <?php echo file_get_contents(ROOT.'template/admin/gridView.html'); ?>
                <?php echo file_get_contents(ROOT.'template/admin/databaseView.html'); ?>
                <?php echo file_get_contents(ROOT.'template/admin/itemsSubpage.html'); ?>
                <?php echo file_get_contents(ROOT.'template/admin/locationsSubpage.html'); ?>
            <?php endif; ?>
        </div>
        <div class="aspect-box">
            <div class="layout">
                <div id="actions"></div>
                <div id="main"></div>
                <?php echo file_get_contents(ROOT.'template/main/map.html'); ?>
                <?php include ROOT.'template/main/sidebar.php'; ?>
                <?php echo file_get_contents(ROOT.'template/main/chat.html'); ?>
            </div>
        </div>
    </body>
</html>

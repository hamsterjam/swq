-- Default location

INSERT INTO locations (name) VALUES
    ('default');

-- Test items

INSERT INTO items (name, stack_limit) VALUES
    ('Tester 1', 1),
    ('Tester 2', 5);

CREATE OR REPLACE FUNCTION find_empty_slot(in_player_id INTEGER)
RETURNS INTEGER AS
$$
DECLARE
    _empty_slot INTEGER;
BEGIN
    SELECT rn INTO _empty_slot
    FROM (
        SELECT
            i.position,
            ROW_NUMBER() OVER (ORDER BY i.position) AS rn
        FROM
            inventory AS i
        WHERE
            i.player_id = in_player_id
    ) AS sq
    WHERE
        sq.position + 1 = sq.rn
    ORDER BY
        sq.position DESC;

    RETURN COALESCE(_empty_slot, 0);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION simple_pickup(in_player_id INTEGER, in_item_id INTEGER, in_count INTEGER)
RETURNS TABLE(id INTEGER, count INTEGER, pos INTEGER, type TEXT) AS
$$
DECLARE
    _empty_slot INTEGER;
    _inventory_id  INTEGER;
BEGIN
    _empty_slot = find_empty_slot(in_player_id);

    INSERT INTO inventory AS i
        (player_id, item_id, count, position)
    SELECT
        in_player_id,
        in_item_id,
        in_count,
        _empty_slot
    RETURNING i.id INTO _inventory_id;

    RETURN QUERY
    SELECT
        _inventory_id,
        in_count,
        _empty_slot,
        'insert' AS TYPE;

    RETURN;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION waterfall_pickup(in_player_id INTEGER, in_item_id INTEGER, in_count INTEGER)
RETURNS TABLE(id INTEGER, count INTEGER, pos INTEGER, type TEXT) AS
$$
DECLARE
    _stack_limit  INTEGER;
    _last_updated INTEGER;
    _left_over    INTEGER;
    _empty_slot   INTEGER;
BEGIN
    -- Get stack limit
    SELECT i.stack_limit INTO _stack_limit
    FROM
        items AS i
    WHERE
        i.id = in_item_id;

    -- Table with accumulation
    CREATE TEMP TABLE accumulated AS (
        SELECT
            i.id,
            i.position,
            SUM(_stack_limit - i.count) OVER (ORDER BY i.position) AS acc
        FROM
            inventory AS i
        WHERE i.item_id = in_item_id
          AND i.player_id = in_player_id
    );

    -- Last updated entry
    SELECT a.id INTO _last_updated
    FROM
        accumulated AS a
    WHERE
        a.acc > in_count
    OR a.acc = (
        SELECT MAX(acc)
        FROM accumulated
    )
    ORDER BY position;

    -- Left Over count
    SELECT in_count - a.acc INTO _left_over
    FROM
        accumulated AS a
    WHERE
        a.acc = (
            SELECT MAX(a.acc)
            FROM accumulated AS a
        )
    ORDER BY a.position;
    _left_over = COALESCE(_left_over, in_count);

    -- Empty inventory slot
    _empty_slot = find_empty_slot(in_player_id);

    -- Update the count in existing rows
    CREATE TEMP TABLE update_count AS
    WITH ret AS (
        UPDATE
            inventory AS i
        SET
            count = CASE
                WHEN a.acc <= in_count THEN _stack_limit
                ELSE _stack_limit + in_count - a.acc END
        FROM
            accumulated AS a
        WHERE a.id = i.id
          AND i.item_id = in_item_id
          AND a.acc > 0
          AND i.count < _stack_limit
          AND (a.acc <= in_count OR a.id = _last_updated)
        RETURNING
            i.id,
            i.count,
            i.position
    )
    SELECT * FROM ret;

    -- Insert new item stack
    IF _left_over > 0 THEN
        CREATE TEMP TABLE insert_new AS
        WITH ret AS (
            INSERT INTO inventory AS i
                (player_id, item_id, count, position)
            SELECT
                in_player_id,
                in_item_id,
                _left_over,
                _empty_slot
            RETURNING
                i.id,
                i.count,
                i.position
        )
        SELECT * FROM ret;
    ELSE
        CREATE TEMP TABLE insert_new(id INTEGER, count INTEGER, position INTEGER);
    END IF;

    RETURN QUERY
    SELECT *, 'change' FROM update_count
    UNION ALL
    SELECT *, 'insert' FROM insert_new;

    DROP TABLE accumulated;
    DROP TABLE update_count;
    DROP TABLE insert_new;

    RETURN;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION pickup(in_player_id INTEGER, in_item_id INTEGER, in_count INTEGER)
RETURNS TABLE(id INTEGER, count INTEGER, pos INTEGER, type TEXT) AS
$$
DECLARE
    _stack_limit  INTEGER;
BEGIN
    -- Get stack limit
    SELECT i.stack_limit INTO _stack_limit
    FROM
        items AS i
    WHERE
        i.id = in_item_id;

    IF _stack_limit > 1 THEN
        RETURN QUERY
        SELECT * FROM waterfall_pickup(in_player_id, in_item_id, in_count);
    ELSE
        RETURN QUERY
        SELECT * FROM simple_pickup(in_player_id, in_item_id, in_count);
    END IF;

    RETURN;
END;
$$ LANGUAGE plpgsql;

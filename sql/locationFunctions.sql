DROP TYPE IF EXISTS direction CASCADE;
CREATE TYPE direction AS ENUM (
    'north',
    'east',
    'south',
    'west'
);

CREATE OR REPLACE FUNCTION unlink_location(in_id INTEGER, in_direction direction)
RETURNS TABLE(id INTEGER, link_north INTEGER, link_east INTEGER, link_south INTEGER, link_west INTEGER) AS
$$
DECLARE
    _findLinked TEXT;
    _removeLink TEXT;
    _column1 TEXT;
    _column2 TEXT;
    _linkedId INTEGER;
BEGIN
    CASE in_direction
        WHEN 'north' THEN
            _column1 = 'link_north';
            _column2 = 'link_south';
        WHEN 'east' THEN
            _column1 = 'link_east';
            _column2 = 'link_west';
        WHEN 'south' THEN
            _column1 = 'link_south';
            _column2 = 'link_north';
        WHEN 'west' THEN
            _column1 = 'link_west';
            _column2 = 'link_east';
    END CASE;

    _findLinked = FORMAT($query$
        SELECT
            l.%I AS id
        FROM
            locations AS l
        WHERE
            l.id = $1;
    $query$, _column1);

    _removeLink = $query$
        UPDATE
            locations AS l
        SET
            %I = null
        WHERE
            l.id = $1
        RETURNING
            l.id, l.link_north, l.link_east, l.link_south, l.link_west
    $query$;

    EXECUTE FORMAT(_findLinked, _column1)
        INTO
            _linkedId
        USING
            in_id;

    RETURN QUERY EXECUTE FORMAT(_removeLink, _column1)
        USING
            in_id;

    RETURN QUERY EXECUTE FORMAT(_removeLink, _column2)
        USING _linkedId;

    RETURN;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION link_locations(in_id1 INTEGER, in_id2 INTEGER, in_direction direction)
RETURNS TABLE(id INTEGER, link_north INTEGER, link_east INTEGER, link_south INTEGER, link_west INTEGER) AS
$$
DECLARE
    _queryFormat TEXT;
    _column1 TEXT;
    _column2 TEXT;
BEGIN
    _queryFormat = $query$
        UPDATE
            locations AS l
        SET
            %I = $1
        WHERE
            l.id = $2
        RETURNING
            l.id, l.link_north, l.link_east, l.link_south, l.link_west;
    $query$;

    CASE in_direction
        WHEN 'north' THEN
            _column1 = 'link_north';
            _column2 = 'link_south';
        WHEN 'east' THEN
            _column1 = 'link_east';
            _column2 = 'link_west';
        WHEN 'south' THEN
            _column1 = 'link_south';
            _column2 = 'link_north';
        WHEN 'west' THEN
            _column1 = 'link_west';
            _column2 = 'link_east';
    END CASE;

    RETURN QUERY
        SELECT
            *
        FROM
            unlink_location(in_id1, in_direction) as ret
        WHERE
            ret.id != in_id1 AND ret.id != in_id2;

    RETURN QUERY EXECUTE FORMAT(_queryFormat, _column1)
        USING
            in_id2,
            in_id1;

    RETURN QUERY EXECUTE FORMAT(_queryFormat, _column2)
        USING
            in_id1,
            in_id2;

    RETURN;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION move(in_id INTEGER, in_direction direction)
RETURNS INTEGER AS
$$
DECLARE
    _linkId INTEGER;
    _column TEXT;
    _getLink TEXT;
BEGIN
    CASE in_direction
        WHEN 'north' THEN _column = 'link_north';
        WHEN 'east'  THEN _column = 'link_east';
        WHEN 'south' THEN _column = 'link_south';
        WHEN 'west'  THEN _column = 'link_west';
    END CASE;

    _getLink = $query$
        SELECT
        l.%I AS id
        FROM
        locations AS l,
        players AS p

        WHERE p.id = $1
        AND p.location_id = l.id
    $query$;

    EXECUTE FORMAT(_getLink, _column)
        INTO
            _linkId
        USING
            in_id;

    IF _linkId IS NOT NULL THEN
        UPDATE
            players AS p
        SET
            location_id = _linkId
        WHERE
            p.id = in_id;
    END IF;

    RETURN _linkId;
END;
$$ LANGUAGE plpgsql;


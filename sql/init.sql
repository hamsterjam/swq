CREATE TABLE IF NOT EXISTS users (
    id SERIAL,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    is_admin BOOLEAN DEFAULT FALSE,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS items (
    id SERIAL,
    name VARCHAR(255) NOT NULL,
    stack_limit INTEGER NOT NULL DEFAULT 1,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS locations (
    id SERIAL,
    link_north INTEGER DEFAULT NULL,
    link_east  INTEGER DEFAULT NULL,
    link_south INTEGER DEFAULT NULL,
    link_west  INTEGER DEFAULT NULL,
    name VARCHAR(255) NOT NULL,
    passable BOOLEAN NOT NULL DEFAULT TRUE,
    location POINT NOT NULL DEFAULT '0, 0',
    PRIMARY KEY (id),
    FOREIGN KEY (link_north) REFERENCES locations(id),
    FOREIGN KEY (link_east)  REFERENCES locations(id),
    FOREIGN KEY (link_south) REFERENCES locations(id),
    FOREIGN KEY (link_west)  REFERENCES locations(id)
);

CREATE TABLE IF NOT EXISTS players (
    id SERIAL,
    user_id INTEGER NOT NULL,
    location_id INTEGER NOT NULL DEFAULT 1,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (location_id) REFERENCES locations(id)
);

CREATE TABLE IF NOT EXISTS inventory (
    id SERIAL,
    player_id INTEGER NOT NULL,
    item_id INTEGER NOT NULL,
    count INTEGER NOT NULL DEFAULT 1,
    position INTEGER NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (player_id) REFERENCES players(id),
    FOREIGN KEY (item_id) REFERENCES items(id),
    UNIQUE (player_id, position)
);

CREATE TABLE IF NOT EXISTS dropped_items (
    id SERIAL,
    location_id INTEGER NOT NULL,
    item_id INTEGER NOT NULL,
    count INTEGER NOT NULL DEFAULT 1,
    dropped BIGINT NOT NULL,
    expires BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (location_id) REFERENCES locations(id),
    FOREIGN KEY (item_id) REFERENCES items(id)
);

\i sql/pickupFunctions.sql
\i sql/locationFunctions.sql

<?php

if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
include_once ROOT.'php/authenticate.php';
include_once ROOT.'php/token.php';
authenticate();

$data = json_decode(file_get_contents('php://input'), true);
$message = htmlspecialchars($data['msg']);
$now = (new DateTime())->getTimestamp();

$ch = curl_init('http://localhost:8080');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

$payload = json_encode([
    'type' => 'msg',
    'time' => $now,
    'data' => [
        'user' => tokenData('user'),
        'msg'  => $message
    ]
]);

curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
curl_close($ch);

reissueToken();

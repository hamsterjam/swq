<?php

if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
include_once ROOT.'php/dbsetup.php';
include_once ROOT.'php/authenticate.php';
include_once ROOT.'php/SimpleManipulator.php';
authenticate();

class MoveManipulator extends SimpleManipulator {
    private string moveSource;

    public function __construct(PDO $pdo) {
        parent::__construct($pdo);

        $this->moveSource = <<<SQL
            SELECT
                COALESCE(move(:id, :direction), p.location_id) AS id
            FROM
                players AS p
            WHERE
                p.id = :id;
        SQL;
    }
}

$manip = new MoveManipulator($pdo);
$manip->autoHandle();

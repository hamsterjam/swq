<?php

if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
include_once ROOT.'php/dbsetup.php';
include_once ROOT.'php/authenticate.php';
include_once ROOT.'php/token.php';

authenticate();
$id = tokenData('id');

$getItems = $pdo->prepare(<<<SQL
    SELECT inventory.id, items.name, inventory.count, inventory.position
    FROM inventory, items
    WHERE inventory.player_id = :playerid
        AND inventory.item_id = items.id
    ORDER BY inventory.position ASC;
SQL);
$getItems->bindParam(':playerid', $id);
$getItems->execute();

$items = [];
while ($next = $getItems->fetch(PDO::FETCH_NUM)) {
    array_push($items, [
        'id'    => $next[0],
        'name'  => $next[1],
        'count' => $next[2],
        'pos'   => $next[3]
    ]);
}

reissueToken();
echo json_encode($items);

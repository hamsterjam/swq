<?php

if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
include_once ROOT.'php/dbsetup.php';
include_once ROOT.'php/authenticate.php';
include_once ROOT.'php/SimpleManipulator.php';
authenticate();

class PickupManipulator extends SimpleManipulator {
    private int $waitTime = 10;

    private string searchSource;
    private string pickupSource;

    public function __construct(PDO $pdo) {
        parent::__construct($pdo);

        $this->searchSource = <<<SQL
            WITH clean_old AS (
                DELETE FROM dropped_items
                WHERE expires < :now
            )
            SELECT
                dropped_items.id AS id,
                items.name AS name,
                count
            FROM
                dropped_items,
                items
            WHERE location_id = :location
              AND items.id = item_id
              AND expires >= :now;
        SQL;

        $this->pickupSource = <<<SQL
            WITH dropped AS (
                DELETE FROM dropped_items AS d
                USING
                    items AS i
                WHERE item_id = i.id
                  AND d.id = :dropped_id
                  AND d.location_id = :location_id
                  AND d.dropped <= :check_till
                  AND d.expires >= :now
                RETURNING
                    i.id AS item_id,
                    d.id AS dropped_id,
                    d.count,
                    i.stack_limit
            )
            SELECT pu.*
            FROM pickup(
                :player_id,
                (SELECT item_id FROM dropped),
                (SELECT count   FROM dropped)
            ) AS pu
            UNION ALL
            SELECT
                dropped_id AS id,
                count,
                NULL AS pos,
                'pickup' AS type
            FROM dropped;
        SQL;

        $this->handlers['search']    = function($data) {return $this->search($data);};
        $this->handlers['searchEnd'] = function($data) {return $this->searchEnd($data);};
        $this->handlers['pickup']    = function($data) {return $this->pickup($data);};
    }

    public function search($data) {
        $now = (new DateTime())->getTimestamp();

        reissueToken([
            'act' => 'search',
            'data' => $now + $this->waitTime
        ]);

        return $this->waitTime;
    }

    public function searchEnd($data) {
        $now = (new DateTime())->getTimestamp();

        if (tokenData('act') !== 'search' || tokenData('data') > $now) {
            http_response_code(403); // Forbidden
            exit();
        }

        $searchArea = $this->pdo->prepare($this->searchSource);
        $searchArea->bindParam(':location', tokenData('loc'));
        $searchArea->bindParam(':now', $now);
        $searchArea->execute();

        reissueToken([
            'act' => 'pickup',
            'data'=> $now
        ]);

        return $searchArea->fetchAll();
    }

    public function pickup($data) {
        $now = (new DateTime())->getTimestamp();

        if (tokenData('act') !== 'pickup') {
            http_response_code(403); // Forbidden
            exit();
        }

        $waterfallPickup = $this->pdo->prepare($this->pickupSource);
        $waterfallPickup->bindParam(':dropped_id', $data, PDO::PARAM_INT, 4);
        $waterfallPickup->bindParam(':player_id', tokenData('id'), PDO::PARAM_INT, 4);
        $waterfallPickup->bindParam(':location_id', tokenData('loc'), PDO::PARAM_INT, 4);
        $waterfallPickup->bindParam(':check_till', tokenData('data'), PDO::PARAM_INT, 8);
        $waterfallPickup->bindParam(':now', $now, PDO::PARAM_INT, 8);
        $waterfallPickup->execute();

        return $waterfallPickup->fetchAll();
    }
}

$manip = new PickupManipulator($pdo);
$manip->autoHandle();

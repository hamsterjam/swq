<?php

if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
include_once ROOT.'php/dbsetup.php';
include_once ROOT.'php/token.php';

$data = json_decode(file_get_contents('php://input'), true);
$username = $data['user'];
$password = $data['pass'];

$checkExisting = $pdo->prepare(<<<SQL
    SELECT id
    FROM users
    WHERE username = :username
    LIMIT 1;
SQL);

$insertNew = $pdo->prepare(<<<SQL
    WITH newUser AS (
        INSERT INTO users (username, password)
        VALUES (:username, :password)
        RETURNING id
    )
    INSERT INTO players (user_id)
    SELECT id
    FROM newUser
    RETURNING id, location_id;
SQL);

// Check for existing user
$checkExisting->bindParam(':username', $username);
$checkExisting->execute();
if ($checkExisting->fetch()) {
    http_response_code(409); // Conflict
    exit();
}

// Insert new user
$insertNew->bindParam(':username', $username);
$insertNew->bindParam(':password', password_hash($password, PASSWORD_DEFAULT));
$insertNew->execute();
$data = $insertNew->fetch(PDO::FETCH_NUM);

header('Authorization: Bearer ' . generateToken([
    'id'   => $data[0],
    'user' => $username,
    'loc'  => $data[1]
]));

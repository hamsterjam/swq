<?php

if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
include_once ROOT.'php/dbsetup.php';
include_once ROOT.'php/token.php';

$data = json_decode(file_get_contents('php://input'), true);
$username = $data['user'];
$password = $data['pass'];

$getExisting = $pdo->prepare(<<<SQL
    SELECT
        p.id,
        p.location_id,
        u.password,
        u.is_admin
    FROM
        users AS u,
        players AS p

    WHERE u.username = :username
      AND p.user_id = u.id

    LIMIT 1;
SQL);

// Get user data
$getExisting->bindParam(':username', $username);
$getExisting->execute();
$userData = $getExisting->fetch();

if (!$userData) {
    http_response_code(401); // Unauthorized
    exit();
}

if (!password_verify($password, $userData['password'])) {
    http_response_code(401); // Unauthorized
    exit();
}

$tokenData = [
    'id'   => $userData['id'],
    'user' => $username,
    'loc'  => $userData['location_id']
];

if ($userData['is_admin']) $tokenData['admin'] = true;

header('Authorization: Bearer ' . generateToken($tokenData));

<?php

if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
include_once ROOT.'php/dbsetup.php';
include_once ROOT.'php/authenticate.php';
include_once ROOT.'php/token.php';

define('DROP_ITEM', <<<SQL
    WITH dropped AS (
        DELETE FROM inventory
        WHERE id = :inventoryId
            AND player_id = :playerId
        RETURNING item_id, count
    )
    INSERT INTO dropped_items (location_id, item_id, count, dropped, expires)
    SELECT location_id, item_id, count, :now, :expires
    FROM dropped, players
    WHERE players.id = :playerId;
SQL);

authenticate();
$playerId = tokenData('id');

$data = json_decode(file_get_contents('php://input'), true);
switch ($data['type']) {
    case 'drop':
        dropItem($data['data'], $playerId);
        break;
}

reissueToken();

function dropItem($invId, $playerId) {
    global $pdo;
    $expireTime = 10*60;
    $now = (new DateTime())->getTimestamp();
    $expires = $now + $expireTime;

    $dropItems = $pdo->prepare(DROP_ITEM);
    $dropItems->bindParam(':playerId', $playerId);
    $dropItems->bindParam(':inventoryId', $invId);
    $dropItems->bindParam(':now', $now);
    $dropItems->bindParam(':expires', $expires);
    $dropItems->execute();
}

<?php

if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
include_once ROOT.'php/dbsetup.php';
include_once ROOT.'php/authenticate.php';
include_once ROOT.'php/DatabaseManipulator.php';
authenticate(true);

class LocationManipulator extends DatabaseManipulator {
    private string $linkSource;
    private string $unlinkSource;

    public function __construct(PDO $pdo) {
        parent::__construct($pdo, 'locations', [
            'name',
            'passable',
            'location',
        ]);

        $this->linkSource = <<<SQL
            SELECT * FROM link_locations(:location1, :location2, :direction);
        SQL;

        $this->unlinkSource = <<<SQL
            SELECt * FROM unlink_location(:id, :direction);
        SQL;

        $this->handlers['link']   = function($data) {return $this->linkLocations($data);};
        $this->handlers['unlink'] = function($data) {return $this->unlinkLocation($data);};
    }

    public function linkLocations($data) {
        $linkLocations = $this->pdo->prepare($this->linkSource);
        $linkLocations->bindParam(':location1', $data['from']);
        $linkLocations->bindParam(':location2', $data['to']);
        $linkLocations->bindParam(':direction', $data['dir']);
        $linkLocations->execute();

        return $linkLocations->fetchAll();
    }

    public function unlinkLocation($data) {
        $unlinkLocation = $this->pdo->prepare($this->unlinkSource);
        $unlinkLocation->bindParam(':id', $data['id']);
        $unlinkLocation->bindParam(':direction', $data['dir']);
        $unlinkLocation->execute();

        return $unlinkLocation->fetchAll();
    }
}

$input = json_decode(file_get_contents('php://input'), true);
$type = $input['type'];
$data = isset($input['data']) ? $input['data'] : null;

$manip = new LocationManipulator($pdo);
$ret = $manip->handle($type, $data);

reissueToken();
echo json_encode($ret);

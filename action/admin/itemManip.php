<?php

if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
include_once ROOT.'php/dbsetup.php';
include_once ROOT.'php/authenticate.php';
include_once ROOT.'php/DatabaseManipulator.php';
authenticate(true);

class ItemManipulator extends DatabaseManipulator {
    private string $takeSource;

    public function __construct(PDO $pdo) {
        parent::__construct($pdo, 'items', [
            'name',
            'stack_limit',
        ]);

        $this->takeSource = <<<SQL
            SELECT * FROM pickup(:player_id, :item_id, :count);
        SQL;

        $this->handlers['take'] = function($data) {return $this->takeItem($data);};
    }

    public function takeItem($data) {
        $takeItem = $this->pdo->prepare($this->takeSource);
        $takeItem->bindParam(':player_id', tokenData('id'));
        $takeItem->bindParam(':item_id', $data['item']);
        $takeItem->bindParam(':count', $data['count']);
        $takeItem->execute();

        return $takeItem->fetchAll();
    }
}

$manip = new ItemManipulator($pdo);
$manip->autoHandle();

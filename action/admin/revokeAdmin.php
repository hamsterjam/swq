<?php

if (!defined('ROOT')) define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
include_once ROOT.'php/authenticate.php';
include_once ROOT.'php/token.php';

authenticate(true);
unset($tokenData['admin']);
reissueToken();
